from src.common.config import app
from src.controller import routes

if __name__ == '__main__':
    app.run(port=6010,debug=True)