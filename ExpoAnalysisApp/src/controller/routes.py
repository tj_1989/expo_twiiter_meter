from src.common.config import app, db
import json
from collections import Counter
from flask import request
from src.models.compare_tweet_by_count import CompareTweetByCount
from src.models.compare_source_by_count import CompareSourceByCount
from src.models.compare_sentiment_by_count import CompareSentiment

tweet_count_compare = CompareTweetByCount()
tweet_source_compare = CompareSourceByCount()
tweet_sentiment_compare = CompareSentiment()

handles_captured = ["@vsexpo1", "@sapientindia","@publicissapient","@bhogleharsha","@sachin_rt","@virendersehwag"]
hashtags_captured = ["#java","#python", "#ml", "#ai", "#rpa", "#cognitivecomputing", "#iot", "#bigdata", "#spark"]
# @app.route('/compare_sources',methods=['POST'])
# def compare_sources():
#     payload = request.get_json(force=True)
#     info = payload['data']

#     results = tweet_source_compare.compare_groups(info)
#     labels = ["Twitter Web Client", "Twitter for iPhone", "Twitter for Android"]
#     count = []
#     print (results)
#     for label in labels:
#         temp = 0
#         for grpname in results:
#             #print (grp)
#             temp += results[grpname][label]
#         count.append(temp)

#     return json.dumps({'labels':labels,'count':count})


@app.route('/compare_sentiment', methods=['GET', 'POST'])
def compare_sentiment():
    if request.method == "POST":
        payload = request.get_json(force=True)
        info = payload['data']

    if request.method == "GET":
        info = handles_captured
    
    label, sentiment = tweet_sentiment_compare.compare_sentiment(info)


    return json.dumps({"label": label, "sentiment": sentiment})


@app.route('/compare_handles', methods=['GET', 'POST'])
def compare_handles():
    if request.method == "POST":
        payload = request.get_json(force = True)
        info = payload['user_mentions']

    if request.method == "GET":
        info = handles_captured

    results = tweet_count_compare.compare_handles(info)
    label = []
    count = []
    for handle in results:
        label.append(handle)
        count.append(results[handle])

    return json.dumps({"label": label, "count": count})

@app.route('/compare_hashtags', methods=['GET', 'POST'])
def compare_hashtags():
    if request.method == "POST":
        payload = request.get_json(force = True)
        info = payload['hashtags']

    if request.method == "GET":
        # info = hashtags_captured
        info = ["#ai", "#bigdata", "#iot", "#rpa"]

    results = tweet_count_compare.compare_hashtags(info)

    return json.dumps(results)

# @app.route('/compare_hashtags', methods=['GET','POST'])
# def compare_hashtags():
#     if request.method == "POST":
#         payload = request.get_json(force = True)
#         info = payload['hashtags']

#     if request.method == "GET":
#         info = ["#java","#python", "#ml", "#ai", "#rpa", "#cognitivecomputing", "#iot", "#trending"]

#     results = tweet_count_compare.compare_hashtags(info)
#     label = []
#     count = []
#     for hashtag in results:
#         label.append(hashtag)
#         count.append(results[hashtag])

#     return json.dumps({"label": label, "count": count})


# @app.route('/compare_groups', methods=['POST'])
# def compare_groups():
#     payload = request.get_json(force = True)
#     info = payload['data']
#     results = tweet_count_compare.compare_groups(info)
    
#     return json.dumps(results)


@app.route('/word_cloud', methods=['GET'])
def word_cloud():
    
    label, count = tweet_count_compare.word_cloud()

    return json.dumps({"label": label, "strength": count})

@app.route('/recent_tweets', methods=['GET'])
def recent_tweets():
    info = hashtags_captured
    results = tweet_count_compare.recent_tweets_count(info)

    return json.dumps(results)