from src.common.config import db
import re
import datetime
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import json
from random import shuffle
import operator

class CompareTweetByCount():
    def __init__(self):
        pass

    # def compare_hashtags(self, info):
    #     if info == 'all':
    #         info = hashtags_captured
        
    #     result = {}
        
    #     for h in info:
    #         if h[0] == "#":
    #             h = h[1:]
    #         pat = re.compile(h, re.I)
    #         count = db.test.find({'hashtags.text': pat }).count()
    #         result[h] = count

    #     return result

    def compare_handles(self, info):
        result = {}
        for handles in info:
            count = db.test.find({'user_mentions.screen_name': re.compile(handles, re.I) if handles[0] != "@" else re.compile(handles[1:], re.I)}).count()
            result[handles] = count

        return result

    # def compare_groups(self, info):
    #     if info == 'all':
    #         info = groups_captured

    #     result = {}
    #     for grp in info.keys():
    #         expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in info[grp]['hashtag']]
    #         expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in info[grp]['handles']]
    #         expression = expression_handles + expression_hashtags
    #         count = db.test.find({"$or" : expression}).count()
    #         result[grp] = count

    #     return result


    def compare_hashtags(self, info):

        result = {}
        
        now = datetime.datetime.now()
        start = now - datetime.timedelta(minutes=60)
        
        deltas = []
        while start < now:
            deltas.append(start)
            start= start+datetime.timedelta(minutes=10)

        for hashtag in info:
            if hashtag not in result:
                result[hashtag]={"time": [], "count": []}

            for t in deltas[:-1]:
                expression1 = {"created_at":{"$gte":t, "$lt":t+datetime.timedelta(minutes=10)}}
                expression2 = {"hashtags.text": re.compile(hashtag, re.I) if hashtag[0] != "#" else re.compile(hashtag[1:], re.I)}
                
                count = db.test.find({"$and": [expression1, expression2] }).count()
                result[hashtag]["count"].append(count)
                result[hashtag]["time"].append(str(t))

        return result


    def word_cloud(self):
        all_hashtags = db.test.distinct('hashtags.text')
        preset_hashtags = ["emotionalintelligence", "ReinforcementLearning", "GlobalBlockChainSummit",\
                           "AzureMachineLearning", "Robotech", "Keras", "AugmentedIntelli", "cryptocurrencymarket",\
                           "machinevision", "PyTorch", "NLP", "ACL", "cryptotrader", "anomalydetection", "cognitivescience", \
                           "nintendo", "bankingindustry", "iotbreakfast", "innovazione", "deepbrainchain", "decsummit", \
                           "androiddeveloper", "deepmind", "xai", "datascientist", "conversationalUI", \
                           "semanticweb", "automl", "azuresearch", "cognitivesearch", "knowledgemining", "autonomouscars", \
                           "aiforsocialgood", "smarttourism", "AIera", "infosecurity", "managedservices","UiPathForward","DeepReinforcementLearning",
                           "cloud", "analytics", "cloudera", "creativecloud"]
        #print (all_hashtags)


        results = {}
        for hashtag in preset_hashtags:
            results[hashtag] = db.test.find({'hashtags.text': re.compile(hashtag, re.I) if hashtag[0] != "#" else re.compile(hashtag[1:], re.I)}).count()

        sorted_results = sorted(results.items(), key=operator.itemgetter(1), reverse=True)

        word = []
        strength = []
        for temp in sorted_results[0:30]:
            word.append(temp[0])
            strength.append(temp[1])

        return word, strength

    def recent_tweets_count(self, info):
        now = datetime.datetime.now()
        start = now - datetime.timedelta(minutes=2)
        
        #expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in info]
        #expression_time = {"created_at":{"$gt": start}}
        #tweets = db.test.find({"$and":[expression_time, {"$or": expression_hashtags}]})
        
        tweets = db.test.find({"created_at":{"$gt": start}})
        tweets_text = []
        tweets_text = [(doc["text"] , doc["name"]) for doc in tweets]
        shuffle(tweets_text)
        if len(tweets_text) >= 4:
            return tweets_text[0:4]
        else:
            return tweets_text