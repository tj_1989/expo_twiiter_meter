from src.common.config import db, hashtags_captured, handles_captured, groups_captured
import re

class CompareSourceByCount():
    def __init__(self):
        self.sources_captured = ["Twitter Web Client", "Twitter for iPhone", "Twitter for Android"]

    def compare_groups(self, info):
        result = {}
        if info == 'all':
            result["all"] = {}
            for s in self.sources_captured:
                result["all"][s] = db.test.find({'source': re.compile(s, re.I)}).count()
        else:
            for grp in info.keys():
                result[grp] = {}
                expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in info[grp]['hashtag']]
                expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in info[grp]['handles']]
                expression = expression_handles + expression_hashtags
                for s in self.sources_captured:
                    result[grp][s] = db.test.find({"$and": [{'source':  re.compile(s, re.I)}, {"$or" : expression}]}).count()

        return result