from src.common.config import db, hashtags_captured, handles_captured, groups_captured
import re


class CompareSentiment():
    def __init__(self):
        self.sentiment_map = {1: "Positive", -1: "Negative", 0: "Neutral"}

    def compare_sentiment(self, info):
        sentiment = []
        label = []
        # if info == "all":
        expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in info]
        for s in self.sentiment_map:
            sentiment.append(db.test.find({"$and": [{'retweeted': False}, {'sentiment': s}, {"$or": expression_handles}]}).count())
            label.append(self.sentiment_map[s])

        # else:
        #     for grp in info.keys():
        #         result[grp] = {}
        #         expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in info[grp]['hashtag']]
        #         expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in info[grp]['handles']]
        #         expression = expression_handles + expression_hashtags
        #         for s in self.sentiment_map:
        #             result[grp][self.sentiment_map[s]] = db.test.find({"$and": [{'retweeted': False}, \
        #                 {'sentiment': s}, \
        #                 {"$or" : expression}]}).count()
        

        return label, sentiment