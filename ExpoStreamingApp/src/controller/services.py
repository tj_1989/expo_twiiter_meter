from src.common.config import client

class DatabaseService:

    def __init__(self,dbname):
        self.db = getattr(client,dbname,None)

    def insert_document(self,collection,doc):
        try:
            table = getattr(self.db,collection)
            result = table.insert_one(doc)
            print (result.inserted_id)
        except Exception as e:
            print (e)

    def find_document(self,collection,query=None):
        try:
            table = getattr(self.db,collection)
            data = table.find(query)
            for d in data:
                print (d)
        except Exception as e:
            print (e)
