from flask_restplus import Resource, fields
from src.models.twitter_streamer import TwitterStreamer
from src.common.config import api
import os

model = api.model('input', {
    'hashtags': fields.List(fields.String)
})


@api.route('/streamtweets')
class StreamTweets(Resource):
    @api.expect(model)
    def post(self):
        streamer = TwitterStreamer()
        request = api.payload
        print('streaming tweets...')
        streamer.stream_tweets(request['hashtags'])
        return {'result': 'success'}


@api.route('/process_images')
class ProcessImages(Resource):
    def get(self):
        files = []
        for dirpath, dirnames, filenames in os.walk(os.path.join(os.getcwd(),'tweet_images')):
            files.extend(filenames)
        return {'files': files}
