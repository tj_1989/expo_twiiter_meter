import urllib, os


def download_image(url):
    file_name = url.split('/')[-1]
    try:
        urllib.request.urlretrieve(url, os.path.join(os.getcwd(), file_name))
    except Exception as e:
        print(str(e))


def add_dict_to_set( unique_list, unique_key, dict_to_add):
    is_present = False
    for e in unique_list:
        if e[unique_key] == dict_to_add[unique_key]:
            is_present = True
    if not is_present:
        unique_list.append(dict_to_add)
    return unique_list


def flatten_hashtags( tweet):
    hashtags = []
    for hashtag in tweet['entities']['hashtags']:
        hashtags = add_dict_to_set(hashtags, 'text', hashtag)
    if 'retweeted_status' in tweet:
        retweeted_status = tweet['retweeted_status']
        for hashtag in retweeted_status['entities']['hashtags']:
            hashtags = add_dict_to_set(hashtags, 'text', hashtag)
        if 'extended_tweet' in retweeted_status:
            extended_tweet = retweeted_status['extended_tweet']
            for hashtag in extended_tweet['entities']['hashtags']:
                hashtags = add_dict_to_set(hashtags, 'text', hashtag)
    return hashtags


def flatten_user_mentions( tweet):
    user_mentions = []
    for user in tweet['entities']['user_mentions']:
        user_mentions = add_dict_to_set(user_mentions, 'screen_name', user)
    if 'retweeted_status' in tweet:
        retweeted_status = tweet['retweeted_status']
        for user in retweeted_status['entities']['user_mentions']:
            user_mentions = add_dict_to_set(user_mentions, 'screen_name', user)
        if 'extended_tweet' in retweeted_status:
            extended_tweet = retweeted_status['extended_tweet']
            for user in extended_tweet['entities']['user_mentions']:
                user_mentions = add_dict_to_set(user_mentions, 'screen_name', user)
    return user_mentions

