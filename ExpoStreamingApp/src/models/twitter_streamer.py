from tweepy import Stream
from src.models.twitter_listener import TwitterListener
from src.models.authenticator import Authenticator

class TwitterStreamer:
    def __init__(self):
        self.listener = TwitterListener(10000)
        self.auth = Authenticator().authenticate()
        self.stream = Stream(self.auth, self.listener)

    def stream_tweets(self, hashtag_list):
        self.stream.filter(track=hashtag_list,async=True)

if __name__ == '__main__':
    streamer = TwitterStreamer()
    hashtags = ['@BJP4India']
    print ('streaming tweets...')
    streamer.stream_tweets(hashtags)

