from src.common import config
from tweepy import OAuthHandler


class Authenticator:
    
    def authenticate(self):
        authenticated = OAuthHandler(config.CONSUMER_KEY, config.CONSUMER_SECRET)
        authenticated.set_access_token(config.ACCESS_TOKEN, config.ACCESS_TOKEN_SECTRET)
        return authenticated
