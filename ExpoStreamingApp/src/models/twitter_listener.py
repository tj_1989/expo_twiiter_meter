from tweepy import StreamListener
from src.controller.services import DatabaseService
from src.models.tweet_analyzer import TweetAnalyzer
from src.common import utils
import json, re
import urllib, os
import dateutil
from datetime import datetime


class TwitterListener(StreamListener):
    """
    A listener class to listen to tweets
    """

    def __init__(self, limit=None):
        self.counter = 0
        self.limit = limit
        self.db_service = DatabaseService('ExpoDB')
        self.tweet_analyzer = TweetAnalyzer()

    def download_image(self, url, folder_name):
        file_name = url.split('/')[-1]
        try:
            urllib.request.urlretrieve(url, os.path.join(os.getcwd(), folder_name, file_name))
        except Exception as e:
            print(str(e))

    def add_dict_to_set(self, unique_list, unique_key, dict_to_add):
        is_present = False
        for e in unique_list:
            if e[unique_key] == dict_to_add[unique_key]:
                is_present = True
        if not is_present:
            unique_list.append(dict_to_add)
        return unique_list

    def flatten_hashtags(self, tweet):
        hashtags = []
        for hashtag in tweet['entities']['hashtags']:
            # hashtags.add(hashtag['text'])
            hashtags = self.add_dict_to_set(hashtags, 'text', hashtag)
        if 'retweeted_status' in tweet:
            retweeted_status = tweet['retweeted_status']
            for hashtag in retweeted_status['entities']['hashtags']:
                hashtags = self.add_dict_to_set(hashtags, 'text', hashtag)
            if 'extended_tweet' in retweeted_status:
                extended_tweet = retweeted_status['extended_tweet']
                for hashtag in extended_tweet['entities']['hashtags']:
                    hashtags = self.add_dict_to_set(hashtags, 'text', hashtag)
        return hashtags

    def flatten_user_mentions(self, tweet):
        user_mentions = []
        for user in tweet['entities']['user_mentions']:
            user_mentions = self.add_dict_to_set(user_mentions, 'screen_name', user)
        if 'retweeted_status' in tweet:
            retweeted_status = tweet['retweeted_status']
            for user in retweeted_status['entities']['user_mentions']:
                user_mentions = self.add_dict_to_set(user_mentions, 'screen_name', user)
            if 'extended_tweet' in retweeted_status:
                extended_tweet = retweeted_status['extended_tweet']
                for user in extended_tweet['entities']['user_mentions']:
                    user_mentions = self.add_dict_to_set(user_mentions, 'screen_name', user)
        return user_mentions

    def get_tweet_images(self, tweet):
        media_objects = []
        if 'media' in tweet['entities']:
            media_objects.extend(tweet['entities']['media'])
        if 'retweeted_status' in tweet:
            retweeted_status = tweet['retweeted_status']
            if 'media' in retweeted_status['entities']:
                media_objects.extend(retweeted_status['entities']['media'])
            if 'extended_tweet' in retweeted_status:
                extended_tweet = retweeted_status['extended_tweet']
                if 'media' in extended_tweet['entities']:
                    media_objects.extend(extended_tweet['entities']['media'])
        urls = set([m['media_url'] for m in media_objects])
        for url in urls:
            self.download_image(url, folder_name='tweet_images')

    def create_doc_from_tweet(self, tweet):
        doc = {}
        retweeted = False
        if 'retweeted_status' in tweet:
            retweeted = True
            retweeted_status = tweet['retweeted_status']
            if 'extended_tweet' in retweeted_status:
                doc['original_tweet'] = retweeted_status['extended_tweet']['full_text']

        doc['text'] = tweet['text']
        user = tweet['user']
        profile_image = user['profile_image_url']
        self.download_image(profile_image, 'profile_img')
        self.get_tweet_images(tweet)
        if 'media' in tweet['entities']:
            print(tweet['entities']['media'])
        doc['name'] = user['name']
        doc['screen_name'] = user['screen_name']
        try:
            doc['created_at'] = datetime.strptime(tweet['created_at'],'%a %b %d %H:%M:%S %z %Y')
        except:
            try:
                doc['created_at'] = dateutil.parse(tweet['created_at'])
            except Exception as e:
                print ('Failed to parse the date. Saving as string')
                doc['created_at'] = tweet['created_at']
        doc['source'] = re.sub(r'<.+?>', '', tweet['source'])
        doc['retweet_count'] = tweet['retweet_count']
        doc['favorite_count'] = tweet['favorite_count']
        doc['retweeted'] = retweeted
        doc['user_mentions'] = self.flatten_user_mentions(tweet)
        doc['hashtags'] = self.flatten_hashtags(tweet)
        doc['sentiment'] = self.tweet_analyzer.analyze_sentiments(tweet['text'])
        # doc = tweet
        return doc

    def on_data(self, data):
        try:
            print('getting tweets')
            json_data = json.loads(data)
            doc = self.create_doc_from_tweet(json_data)
            self.db_service.insert_document('test', doc)
            if self.limit:
                if self.counter < self.limit:
                    self.counter += 1
                    return True
                else:
                    print('limit crossed..')
                    return False
            return True


        except Exception as e:
            raise Exception('Cannot stream tweets')

    def on_error(self, status):
        raise Exception('Something went wrong. Error code: ' + str(status))

    def find_doc(self, col):
        self.db_service.find_document(col)


if __name__ == '__main__':
    listener = TwitterListener()
    listener.find_doc('test')
