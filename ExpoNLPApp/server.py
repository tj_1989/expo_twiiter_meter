import logging
from app import create_app

app = create_app()

if __name__ == '__main__':
	logger = logging.getLogger('expo-NLP-logging')
	logger.info('NLP QnA Services Started')

	app.run(host='0.0.0.0', port=5010, threaded=True)
