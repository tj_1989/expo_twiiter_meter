import logging
logger = logging.getLogger('expo-NLP-logging')


class InvalidData(Exception):
	# Invalid Data exception
    def __init__(self, message):
        Exception.__init__(self)
        self.message = message
        logger.error('InvalidData: ' + message)
