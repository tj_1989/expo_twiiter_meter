import string

class Counting():
    def __init__(self):
        self.devices = {"Twitter Web Client": ["laptop", "laptops",  "web client", "twitter web client", "local machine"], \
                        "Twitter for iPhone": ["iphone", "iphones", "ios", "apple phone", "apple device", "apple iphone"], \
                        "Twitter for Android": ["andriod", "android device", "android phone", "google devics", "androids"]}

    def get_hashtags_and_handles(self, text):
        inp = self.preprocess_text(text)
        inp = inp.split()
        hashtags = [word for word in inp if word.startswith("#")]
        replyats = [word for word in inp if word.startswith("@")]
        return hashtags, replyats


    def get_devices(self, text):
        result = []
        inp = self.preprocess_text(text)
        for dkeys in self.devices:
            for dvalues in self.devices[dkeys]:
                if dvalues in text:
                    result.append(dkeys)
        return list(set(result))
        
    def preprocess_text(self, text):
	# cleans the text of invalid and insignificant characters
        text = text.replace('\n', ' ')
        text = text.replace('\t', ' ')
        text = text.lower()
	# remove punctuations except # and @ as they are significant
        exclude = set(string.punctuation)
        exception = set(['@','#'])
        exclude -= exception
        text = ''.join(ch for ch in text if ch not in exclude)
        return text
