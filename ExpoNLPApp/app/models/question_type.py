import fasttext
from nltk.tokenize import word_tokenize

class QuestionType():
    def __init__(self):
        self.classifier = fasttext.load_model('app/trained_models/best_model.bin', label_prefix='__label__')
        self.label_dict = {1: 'count', 2: 'handle', 3: 'tweet'}

    def get_question_type(self, text):
        text = text.strip().lower()
        text = ''.join([i if ord(i) < 128 else '' for i in text])
        text = " ".join(word_tokenize(text))
        prediction = self.classifier.predict([text])[0][0]
        pred_label = self.label_dict[int(prediction[0])]

        return pred_label

