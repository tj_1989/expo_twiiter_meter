from flask import Flask
import logging
from logging.handlers import TimedRotatingFileHandler

debug = True

log_level = logging.DEBUG if debug else logging.ERROR
logger = logging.getLogger('expo-NLP-logging')

file_handler = TimedRotatingFileHandler('logs/expo-NLP-logging.log', 'D', 1, 10)
file_handler.setLevel(log_level)
file_handler.setFormatter(logging.Formatter(
	'%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))

logger.addHandler(file_handler)
logger.setLevel(log_level)

def create_app():
	app = Flask(__name__)
	app.config['ENV'] = 'development'
	app.config['DEBUG'] = True

	from .routes import routes
	app.register_blueprint(routes)
	
	from .routes import exceptions
	app.register_error_handler(exceptions.InvalidUsage, exceptions.invalid_usage_handler)

	return app
