import json
import logging
from flask import request, jsonify
from . import routes
from .exceptions import InvalidUsage
from app.models.question_type import QuestionType
from app.models.count import Counting
from app.common.config import db
import re
from random import shuffle

qt = QuestionType()
count_entities = Counting()


logger = logging.getLogger('expo-NLP-logging')
all_devices =  ["Twitter Web Client", "Twitter for iPhone", "Twitter for Android"]
all_hashtags = ["#python", "#java", "#c", "#nodejs"]
all_handles = ["@bjp4india"]


@routes.route('/qna', methods=['POST'])
def qna():
    # check if the payload is in valid format
    payload = request.get_json(force=True)
    question = payload["text"]
	
    question_type = qt.get_question_type(question)
    if question_type == "count":
        print ("Return Count")
        result = get_count(question)
    elif question_type == "tweet": 
        result = get_tweet(question)
    elif question_type == "handle":
        result = get_handle(question)
    else:
        result = "Apologies, I did not get your query"

    return jsonify(success = result)

def get_handle(question):
    hashtags, handles = count_entities.get_hashtags_and_handles(question)
    devices = count_entities.get_devices(question)
    print ("hashtags found %s" ",".join(hashtags))
    print ("handles found %s" ",".join(handles))
    print ("devices found %s" ",".join(devices))

    if len(hashtags) == 0 and len(handles) == 0:
        expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in all_hashtags]
        expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in all_handles]
        expression_un = expression_hashtags + expression_handles

    if len(hashtags) != 0 and len(handles) == 0:
        expression_un = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in hashtags]
        
    if len(hashtags) == 0 and len(handles) != 0:
        expression_un = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in handles]
        
    if len(hashtags) != 0 and len(handles) != 0:
        expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in hashtags]
        expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in handles]
        expression_un = expression_hashtags + expression_handles

    if len(devices) == 0:
        devices = all_devices

    expression_devices = [{'source': re.compile(i, re.I)} for i in devices]
    print ("Exporession Device")    
    print (expression_devices)
    print ("Expression Handles and Hashtags")
    print (expression_un)
    tweets = db.test.find({"$and": [{"$or": expression_un}, {"$or": expression_devices}, {"retweeted": False}]})
    tweets_handles = [doc["name"] for doc in tweets]
    
    shuffle(tweets_handles)
    
    if len(tweets_handles) < 5:
        return tweets_handles
    else:
        return tweets_handles[0:5]
    


def get_tweet(question):
    hashtags, handles = count_entities.get_hashtags_and_handles(question)
    devices = count_entities.get_devices(question)
    print ("hashtags found %s" ",".join(hashtags))
    print ("handles found %s" ",".join(handles))
    print ("devices found %s" ",".join(devices))
    
    if len(hashtags) == 0 and len(handles) == 0:
        expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in all_hashtags]
        expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in all_handles]
        expression_un = expression_hashtags + expression_handles

    if len(hashtags) != 0 and len(handles) == 0:
        expression_un = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in hashtags]
        
    if len(hashtags) == 0 and len(handles) != 0:
        expression_un = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in handles]
        
    if len(hashtags) != 0 and len(handles) != 0:
        expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in hashtags]
        expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in handles]
        expression_un = expression_hashtags + expression_handles

    if len(devices) == 0:
        devices = all_devices

    expression_devices = [{'source': re.compile(i, re.I)} for i in devices]
    print ("Exporession Device")    
    print (expression_devices)
    print ("Expression Handles and Hashtags")
    print (expression_un)
    tweets = db.test.find({"$and": [{"$or": expression_un}, {"$or": expression_devices}, {"retweeted": False}]})
    tweets_text = [doc["text"] for doc in tweets if len(doc["text"]) >= 10]
    shuffle(tweets_text)
    
    if len(tweets_text) < 5:
        return tweets_text
    else:
        return tweets_text[0:5]
    
    
def get_count(question):
    hashtags, handles = count_entities.get_hashtags_and_handles(question)
    print (hashtags)
    print (handles)
    print (all_hashtags)
    print (all_handles) 
    devices = count_entities.get_devices(question)
    print ("hashtags found %s" %",".join(hashtags))
    print ("handles found %s" %",".join(handles))
    print ("devices found %s" %",".join(devices))
    if len(hashtags) == 0 and len(handles) == 0:
        expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in all_hashtags]
        expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in all_handles]
        expression_un = expression_hashtags + expression_handles

    elif len(hashtags) != 0 and len(handles) == 0:
        expression_un = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in hashtags]
        
    elif len(hashtags) == 0 and len(handles) != 0:
        expression_un = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in handles]
        
    else:
	#len(hashtags) != 0 and len(handles) != 0:
        expression_hashtags = [{'hashtags.text' : re.compile(i, re.I) if i[0] != "#" else re.compile(i[1:], re.I)} for i in hashtags]
        expression_handles = [{'user_mentions.screen_name' : re.compile(i, re.I) if i[0] != "@" else re.compile(i[1:], re.I)} for i in handles]
        expression_un = expression_hashtags + expression_handles
 
	
    if len(devices) == 0:
        devices = all_devices

    expression_devices = [{'source': re.compile(i, re.I)} for i in devices]
    print ("Exporession Device")    
    print (expression_devices)
    print ("Expression Handles and Hashtags")
    print (expression_un)

    expression_time = {'created_at': { "$gt": 'new Date("2017-07-10T15:00:00")'}}

    count = db.test.find({"$and": [{"$or": expression_un}, {"$or": expression_devices}]}).count()
    #count = db.test.find(expression_time).count()

    return count
