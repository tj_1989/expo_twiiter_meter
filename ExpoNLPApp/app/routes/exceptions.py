import logging
from flask import jsonify
logger = logging.getLogger('expo-NLP-logging')

class InvalidUsage(Exception):
	status_code = 400

	def __init__(self, message, status_code=None, payload=None):
		Exception.__init__(self)
		self.message = message
		if status_code is not None:
			self.status_code = status_code
		self.payload = payload
		logger.error(message)

	def to_dict(self):
		rv = dict(self.payload or ())
		rv['error'] = self.message
		return rv

def invalid_usage_handler(error):
	response = jsonify(error.to_dict())
	response.status_code = error.status_code
	return response
