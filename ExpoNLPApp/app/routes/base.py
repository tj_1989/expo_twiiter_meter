from flask import Blueprint
from flask import jsonify
import logging

routes = Blueprint('routes', __name__)
logger = logging.getLogger('text-summariztion-logging')


@routes.route('/')
def index():
	"""
	Base route
	"""
	logger.info('Text Summarization Service Started')
	return jsonify(success='Text Summarization Service Running')
