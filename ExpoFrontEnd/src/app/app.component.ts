import { Component, OnInit } from '@angular/core';

import { Chart } from 'node_modules/chart.js';
import { AgWordCloudData, AgWordCloudDirective } from 'node_modules/angular4-word-cloud';
import { ChartsDataService } from './ChartsData.sevice';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  question = '';
  title = 'Twitter-Meter';
  LineChart = [];
  DoughNutChart = [];
  BarChart = [];
  PolarAreaChart = [];
  lineChartLabels = [];
  lineChartData = [];
  barChartLabels = [];
  barChartData = [];
  doughnutChartLabels = [];
  doughnutChartData = [];
  polarAreaChartLabels = [];
  polarAreaChartData = [];
  answer = '';
  timeInterval = 120;
  recentTweets: any;
  @ViewChild(AgWordCloudDirective) agWordCloudDirective: AgWordCloudDirective;

  wordData: Array<AgWordCloudData> = [
    { size: 500, text: 'vitae' },
    { size: 301, text: 'amet' },
    { size: 123, text: 'sit' },
    { size: 321, text: 'eget' },
    { size: 231, text: 'quis' },
    { size: 123, text: 'sem' },
    { size: 346, text: 'massa' },
    { size: 107, text: 'nec' },
    { size: 436, text: 'sed' },
    { size: 731, text: 'semper' },
    { size: 80, text: 'scelerisque' },
    { size: 96, text: 'egestas' },
    { size: 531, text: 'libero' },
    { size: 109, text: 'nisl' },
    { size: 972, text: 'odio' },
    { size: 213, text: 'tincidunt' },
    { size: 294, text: 'vulputate' },
    { size: 472, text: 'venenatis' },
    { size: 297, text: 'malesuada' },
    { size: 456, text: 'finibus' },
    { size: 123, text: 'tempor' },
    { size: 376, text: 'tortor' },
    { size: 93, text: 'congue' },
    { size: 123, text: 'possit' },
  ];

  options = {
    settings: {
      minFontSize: 10,
      maxFontSize: 100,
    },
    margin: {
      top: 10,
      right: 10,
      bottom: 10,
      left: 10
    },
    labels: false // false to hide hover labels
  };

  constructor(private chartsDataService: ChartsDataService) {

  }

  ngOnInit() {
    this.updateRecentTweets();
    this.UpdateBarChartData();
    this.UpdateLineChartData();
    this.UpdateDoughnutChartData();
    this.updateWordCloudData();

    setInterval(() => { this.UpdateLineChartData(); }, this.timeInterval * 1000);

    setInterval(() => { this.UpdateBarChartData(); }, this.timeInterval * 1000);

    setInterval(() => { this.UpdateDoughnutChartData(); }, this.timeInterval * 1000);

    setInterval(() => { this.UpdatePolarAreaChartData(); }, this.timeInterval * 1000);

    setInterval(() => { this.updateRecentTweets(); }, this.timeInterval * 1000);

    setInterval(() => { this.updateWordCloudData(); }, this.timeInterval * 1000);
  }

  updateRecentTweets() {
    this.chartsDataService.getRecentTweets().subscribe(
      data => {
        this.recentTweets = [];
        this.recentTweets = data;
        this.recentTweets = this.recentTweets.slice(0, 4);
      }
    );
  }

  updateWordCloudData() {
    this.chartsDataService.getWordCloudChartData().subscribe(
      data => {
        console.log(data);
        const tempArr = [];
        for (let i = 0; i < data.label.length; i++) {
          tempArr.push({ size: data.strength[i], text: data.label[i] });
        }
        this.agWordCloudDirective.wordData = tempArr;
        this.agWordCloudDirective.update();
      }
    );
  }
  UpdateLineChartData() {
    this.chartsDataService.getLineChartData().subscribe(
      data => {
        let obj = data;
        let lables = [];
        let dataSets = [];
        for (let i = 0; i < 4; i++) {
          lables.push(obj['#ai']['time'][i].substr(11, 8))
        }
        dataSets.push(
          {
            label: 'ai',
            data: obj['#ai']['count'].slice(0, 4),
            borderColor: 'red',
            backgroundColor: 'red',
            fill: false,
            yAxisID: 'y-axis-1',
          },
          {
            label: 'bigdata',
            data: obj['#bigdata']['count'].slice(0, 4),
            borderColor: 'blue',
            backgroundColor: 'blue',
            fill: false,
            yAxisID: 'y-axis-2',
          },
          {
            label: 'iot',
            data: obj['#iot']['count'].slice(0, 4),
            borderColor: 'black',
            backgroundColor: 'black',
            fill: false,
            yAxisID: 'y-axis-3',
          },
          {
            label: 'rpa',
            data: obj['#rpa']['count'].slice(0, 4),
            borderColor: 'orange',
            backgroundColor: 'orange',
            fill: false,
            yAxisID: 'y-axis-4',
          }
        );
        this.lineChartLabels = lables;
        this.lineChartData = dataSets;
        console.log(this.lineChartData);
        console.log(this.lineChartLabels);
        this.CreateLineChart();
      }
    );
  }

  UpdateBarChartData() {
    this.chartsDataService.getBarChartData().subscribe(
      data => {
        this.barChartLabels = data.label;
        this.barChartData = data.count;
        this.CreateBarChart();
      }
    );
  }

  UpdateDoughnutChartData() {
    this.chartsDataService.getDoughnutChartData().subscribe(
      data => {
        this.doughnutChartLabels = data.label;
        this.doughnutChartData = data.sentiment;
        this.CreateDoughnutChart();
      }
    );
  }

  UpdatePolarAreaChartData() {
    this.chartsDataService.getPolarAreaChartData().subscribe(
      data => {
        this.polarAreaChartLabels = data.labels;
        this.polarAreaChartData = data.data;
        this.CreatePolarAreaChart();
      }
    );
  }
  CreateLineChart() {
    if (this.lineChartData !== undefined && this.lineChartLabels !== undefined) {
      this.LineChart = new Chart('lineChart', {
        type: 'line',
        data: {
          labels: this.lineChartLabels,
          datasets: this.lineChartData
        },
        options: {
          responsive: true,
          hoverMode: 'index',
          stacked: false,
          title: {
            display: true,
            text: '#tweets on topics'
          },
          scales: {
            yAxes: [{
              type: 'linear',
              display: true,
              id: 'y-axis-1',
            }, {
              type: 'linear',
              display: true,
              id: 'y-axis-2'
            }, {
              type: 'linear',
              display: true,
              id: 'y-axis-3'
            }, {
              type: 'linear',
              display: true,
              id: 'y-axis-4'
            }],
          }
        }

      });
    }
  }

  CreateBarChart() {

    if (this.barChartData !== undefined && this.barChartLabels !== undefined) {
      this.BarChart = new Chart('barChart', {
        type: 'horizontalBar',
        data: {
          labels: this.barChartLabels,
          datasets: [{
            label: '#tweets on hanldes ',
            data: this.barChartData,
            backgroundColor: [
              'rgba(0, 99, 132, 0.6)',
              'rgba(30, 99, 132, 0.6)',
              'rgba(60, 99, 132, 0.6)',
              'rgba(90, 99, 132, 0.6)',
              'rgba(120, 99, 132, 0.6)',
              'rgba(150, 99, 132, 0.6)'
            ],
            borderColor: [
              'rgba(0, 99, 132, 1)',
              'rgba(30, 99, 132, 1)',
              'rgba(60, 99, 132, 1)',
              'rgba(90, 99, 132, 1)',
              'rgba(120, 99, 132, 1)',
              'rgba(150, 99, 132, 1)'
            ],
            borderWidth: 2,
            hoverBorderWidth: 0
          }]
        },
        options: {
          title: {
            display: false
          },
          scales: {
            yAxes: [{
              barPercentage: 4,
              categoryPercentage: 0.2
            }],
            xAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          },
          elements: {
            rectangle: {
              borderSkipped: 'left',
            }
          }
        }

      });
    }
  }

  CreateDoughnutChart() {
    if (this.doughnutChartData != undefined && this.doughnutChartLabels != undefined) {
      this.DoughNutChart = new Chart('doughnutChart', {
        type: 'doughnut',
        data: {
          labels: this.doughnutChartLabels,
          datasets: [{
            label: "Points",
            backgroundColor: ['#f1c40f', '#e67e22', 'blue', 'green', 'red', 'pink'],
            data: this.doughnutChartData
          }]
        },
        options: {
          title: {
            text: "Sentiments Analysis",
            display: true
          },
          cutoutPercentage: 60,
          animation: {
            animateScale: true
          }
        }

      });
    }
  }

  CreatePolarAreaChart() {
    if (this.polarAreaChartData != undefined && this.polarAreaChartLabels != undefined) {
      this.PolarAreaChart = new Chart('polarAreaChart', {
        type: 'polarArea',
        data: {
          labels: this.polarAreaChartLabels,
          datasets: [{
            label: "Number of Items sold in Months",
            data: this.polarAreaChartData,
            backgroundColor: [
              'rgba(255, 5, 5, 0.2)',
              'rgba(130, 176, 288, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 2
          }]
        },
        options: {
          title: {
            text: "Polar Area Chart",
            display: true
          },
          scales: {
            // yAxes: [{
            //   ticks:{
            //     beginAtZero: true
            //   }
            // }]
            display: false
          }

        }

      });
    }
  }

  askQuestion() {
    this.chartsDataService.askQuestion(this.question).subscribe(
      data => {
        this.answer = data.success;
      }
    );
  }
}
