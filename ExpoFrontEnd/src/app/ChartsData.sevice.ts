import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
// import { map } from "rxjs/operators";
// import { Observable } from "rxjs";
// import 'rxjs/Rx';


// export interface ChartDataModel{
//         labels: string[];
//         data: number[];
// }

@Injectable()
export class ChartsDataService{

    
    constructor(private httpClient: HttpClient){

    }

    getLineChartData(){
       return this.httpClient.get<any>("/api/compare_hashtags");
    }

    getBarChartData(){
        return this.httpClient.get<any>("/api/compare_handles");
     }

     getDoughnutChartData(){
        return this.httpClient.get<any>("/api/compare_sentiment");
     }

     getPolarAreaChartData(){
        return this.httpClient.get<any>("/api/compare_hashtags");
     }

     getWordCloudChartData() {
        return this.httpClient.get<any>("/api/word_cloud");
     }

     askQuestion(question) {
         return this.httpClient.post<any>('/qna', {'text' : question});
     }

     getRecentTweets() {
         return this.httpClient.get('/api/recent_tweets');
     }
}