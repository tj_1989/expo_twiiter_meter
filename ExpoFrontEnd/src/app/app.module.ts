import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { AgWordCloudModule } from 'node_modules/angular4-word-cloud';
// import { DoughnutChartComponent, PieChartComponent, BarChartComponent } from 'angular-d3-charts';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ChartsDataService } from './ChartsData.sevice';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AgWordCloudModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [ChartsDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
